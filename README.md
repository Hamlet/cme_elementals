### CME Elementals
![CME Elementals' screenshot](screenshot.png)  
**_Adds elemental monsters._**

**Version:** 0.1.0  
**Source code's license:** [EUPL v1.2][1] or later  
**Textures and models license:** [CC0 1.0 Universal][2] or later

**Dependencies:** default (found in [Minetest Game][3]), creatures ([CME][4])


### Installation

Unzip the archive, rename the folder to cme_elementals and place it in  
../minetest/mods/

If you only want this to be used in a single world, place it in  
../minetest/worlds/WORLD_NAME/worldmods/

GNU+Linux - If you use a system-wide installation place it in  
~/.minetest/mods/

For further information or help see:  
https://wiki.minetest.net/Help:Installing_Mods


[1]: https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863
[2]: https://creativecommons.org/publicdomain/zero/1.0/
[3]: https://github.com/minetest/minetest_game
[4]: https://forum.minetest.net/viewtopic.php?t=8638
