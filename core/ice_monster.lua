--[[
	CME Elementals - Adds elemental monsters
	Copyright © 2020 Hamlet and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the 'Licence');
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	'AS IS' basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


--
-- Function
--

-- Used for localization
local S = minetest.get_translator('cme_elementals')


--
-- Mob definition
--

creatures.register_mob({
	name = 'cme_elementals:ice_monster',
	stats = {
		hp = 20,
		hostile = true,
		lifetime = 1200,	-- 20 minutes
		can_jump = 1.1,		-- nodes
		can_burn = true,
		has_knockback = false,
		light = {min = 7, max = 15}		-- Burning light
	},

	modes = {
		idle = {
			chance = 0.5,		-- 50%
			duration =	15, 	-- seconds
			moving_speed = 0,
			update_yaw = 5,		-- seconds
		},

		walk = {
			chance = 0.5,		-- 50%
			duration =	120, 	-- 2 minutes
			moving_speed = 2,	-- half the player's speed
			update_yaw = 30,	-- seconds
		},

		attack = {
			chance = 0.0,
			moving_speed = 4	-- player's speed
		},
	},

	model = {
		mesh = 'cme_elementals_sand_monster.b3d',
		textures = {'cme_elementals_ice_monster.png'},
		collisionbox = {-0.3, -1.0, -0.3, 0.3, 0.75, 0.3},
		rotation = -90.0,
		animations = {
			idle = {start = 0, stop = 39, speed = 15},

			walk = {start = 41,	stop = 72, speed = 20},

			attack = {start = 74, stop = 105, speed = 30}
		}
	},

	drops = {
		{'default:ice', {min = 1, max = 2}, chance = 0.33}
	},

	combat = {
		attack_damage = 2,		-- 1 player's heart
		attack_speed = 1.0,		-- seconds
		attack_radius = 3,		-- nodes

		search_enemy = true,
		search_timer = 15,		-- seconds
		search_radius = 16,		-- nodes
		search_type = 'player'
	},

	spawning = {
		abm_nodes = {
			spawn_on = {'default:ice', 'default:snowblock'}
		},

		abm_interval = 45,	-- seconds
		abm_chance = 7000,	-- 1 in 7000
		max_number = 2,		-- per mapblock
		number = {min = 1, max = 2},
		time_range = {min = 19250, max = 4700},	-- Dusk and dawn
		light = {min = 0, max = 6},
		height_limit = {min = -30912, max = 31000},

		spawn_egg = {
			description = S('Ice monster')
		},

		spawner = {
			range = 7,		-- nodes
			number = 2,		-- max mobs
			description = S('Ice monster spawner'),
			light = {min = 0, max = 6}
		}
	}
})
